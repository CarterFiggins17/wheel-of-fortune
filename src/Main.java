
import javafx.application.Application;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main extends Application {
    Pane pane = new Pane();
    private String showWord = "";
    private String wrongLetters = "";
    private String wordPhrase = "";
    private Text wrongText;
    private Text showText;
    private Text results;
    private Button spin;
    private Text scoreText;
    private int score;
    private String nameOfFile = "Phrases.txt";
    private TextField answer;
    private Button yes;
    private Button no;
    private Text again;
    private Button btCheck;
    private Text winner;
    private int numberRight;
    private int addScore;
    private int landedArc = 0;
    private TheWheel wheel;

    @Override
    public void start(Stage primaryStage) {

        //makes the wheel
        wheel = new TheWheel(pane);
        // yes button
        yes = new Button("YES");
        yes.setLayoutX(50);
        yes.setLayoutY(500);
        pane.getChildren().add(yes);

        // no button
        no = new Button("NO");
        no.setLayoutX(90);
        no.setLayoutY(500);
        pane.getChildren().add(no);

        spin = new Button("Spin");
        spin.setLayoutX(500);
        spin.setLayoutY(450);
        pane.getChildren().add(spin);
        spin.setVisible(true);

        //button to submit answer
        btCheck = new Button("Guess");
        btCheck.setLayoutX(380);
        btCheck.setLayoutY(600);
        pane.getChildren().add(btCheck);
        btCheck.setVisible(false);



        score = 0;
        scoreText = new Text("Score: "+ score);
        scoreText.setFont(Font.font(20));
        scoreText.setX(20);
        scoreText.setY(40);
        pane.getChildren().add(scoreText);

        // redraws game
        reDraw();

        //actions for yes and no
        yes.setOnAction(e -> {
            reDraw();
        });
        no.setOnAction(e ->{
            System.exit(0);
        });

        spinButton();
        guessButton();

        //Making a wrapperPane
        BorderPane wrapperPane = new BorderPane();
        wrapperPane.setCenter(wheel.getWheelPane());
        wrapperPane.getChildren().add(pane);

        // Making a Scene
        Scene scene = new Scene(wrapperPane, 650, 650);
        primaryStage.setTitle("HW_6");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public void reDraw(){

        // removes words
        pane.getChildren().remove(wrongText);
        pane.getChildren().remove(showText);
        pane.getChildren().remove(results);
        pane.getChildren().remove(again);
        pane.getChildren().remove(answer);
        pane.getChildren().remove(winner);

        // redraw the word
       wordPhrase = randomWord();
       showWord = startWord(wordPhrase);
       showText = new Text(showWord);
        showText.setY(570);
        showText.setX(50);
        getFont();
        pane.getChildren().add(showText);

        // redraw wrong letters
        wrongLetters = "Wrong Letters: ";
        wrongText = new Text(wrongLetters);
        wrongText.setY(600);
        wrongText.setX(50);
        wrongText.setFont(Font.font(20));
        pane.getChildren().add(wrongText);

        results = new Text("");
        results.setY(590);
        results.setX(350);
        results.setFont((Font.font(15)));
        pane.getChildren().add(results);

        // answer field
        answer = new TextField();
        answer.setLayoutX(345);
        answer.setLayoutY(600);
        answer.setPrefWidth(30);
        pane.getChildren().add(answer);

        //When player wins the game.
        winner= new Text("WINNER");
        winner.setX(50);
        winner.setY(100);
        winner.setFont(Font.font(100));
        winner.setFill(Color.GREEN);

        score = 0;
        scoreText.setText("Score: " + score);

        yes.setVisible(false);
        no.setVisible(false);
        btCheck.setVisible(false);
        spin.setVisible(true);
    }

    // make the font size change due to how long it is.
    public void getFont(){
        if(showWord.length() > 20){
            showText.setFont(Font.font(20));
        }
        else if(showWord.length() > 15){
            showText.setFont(Font.font(35));
        }
        else {
            showText.setFont(Font.font(50));
        }
    }



    // returns a randomWord from a file
    public String randomWord(){
        String filename =  nameOfFile;
        List<String> temp = new ArrayList<>();
        try {
            Scanner file = new Scanner(new File(filename));
            String line;

            while(file.hasNext()){
                line = file.nextLine();
                temp.add(line);
            }
            file.close();
        }
        catch(Exception e){
            System.err.format("Error when reading '%s'.",filename);
            return "Figgins the Great";
        }
        String[] wordArray = temp.toArray(new String[0]);

        int randomNum = (int)(Math.random()* wordArray.length) ;
        return wordArray[randomNum];
    }

    // checks if there is a match.
    public boolean wordCheck(String word, String letterGuess){
        boolean match = false;
        if(letterGuess != "") {
            for (int i = 0; i < word.length(); i++) {
                if (word.toLowerCase().charAt(i) == letterGuess.toLowerCase().charAt(0)) {
                    match = true;
                }
            }
        }

        return match;
    }

    //Changes the word into dashes --- and returns it.
    public String startWord(String word){
        char[] letters = word.toCharArray();;
        String newWord = "";
        for( int i = 0; i < word.length(); i++){
            if(letters[i] != ' '){
                letters[i] = '-';
            }
            newWord = newWord + letters[i];
        }

        return newWord;
    }

    //if answer is Right then changes it and returns
    public String answerRight(String showWord, String word, String answer){
        String show = "";
        char[] showWordArray = showWord.toUpperCase().toCharArray();
        char[] wordArray = word.toUpperCase().toCharArray();
        char[] answerChar = answer.toUpperCase().toCharArray();
        int count = 0;

        for(int i = 0; i < word.length(); i++){
            if(wordArray[i] != ' ' && wordArray[i] == answerChar[0] ){
                if(showWordArray[i] == answerChar[0]){

                }
                else {
                    showWordArray[i] = wordArray[i];
                    count++;
                }
            }
            show = show + showWordArray[i];
        }

        numberRight = count;

        return show;
    }

    // check if game has been won. return true game is over
    public boolean checkGame(String showWord){
        char[] showWordArray = showWord.toCharArray();

        for(int i = 0; i < showWord.length(); i++){
            if(showWordArray[i] != ' '){
                if(showWordArray[i] == '-'){
                    return false;
                }
            }
        }
        return true;
    }

    // end of game logic
    public void playAgain(Pane pane){


        again = new Text("Play Again?");
        again.setX(50);
        again.setY(490);
        again.setFont(Font.font(25));
        again.setFill(Color.BLUE);
        pane.getChildren().add(again);
        yes.setVisible(true);
        no.setVisible(true);

    }



    // what happens when guess button is clicked
    public void guessButton(){
        btCheck.setOnAction(e -> {
            if (answer.getText().compareTo("") == 0) {

            }
            else {
                if (wordCheck(wordPhrase, answer.getText())) {
                    showWord = answerRight(showWord, wordPhrase, answer.getText());
                    showText.setText(showWord);

                    addScore = numberRight;
                    int point = wheel.getPoints();
                    score = score + (addScore * point);
                    int madePoints = addScore * point;
                    results.setText("Correct! There are " + numberRight + " " + answer.getText().toUpperCase() + ". Points Made: " + madePoints);
                    scoreText.setText("Score: " + score);



                    // checks if the game is won then prints winner
                    if (checkGame(showWord)) {
                        //winner logic
                        winner.setText("Score: " + score);
                        pane.getChildren().add(winner);
                        btCheck.setVisible(false);
                        playAgain(pane);
                    }
                    answer.clear();
                }
                else {
                    //prints wrongLetters.
                    wrongLetters = wrongLetters + answer.getText(0, 1).toUpperCase();
                    wrongText.setText(wrongLetters);
                    results.setText("Wrong");
                    score = 0;
                    scoreText.setText("Score: " + score);

                    answer.clear();
                }
                btCheck.setVisible(false);
                spin.setVisible(true);

            }
        });
    }

    // spins the wheel
    public void spinButton(){
        spin.setOnAction(event -> {
            wheel.spinTheWheel();
            spin.setVisible(false);
            btCheck.setVisible(true);
        });


    }


    public static void main(String[] args) {
        launch(args);
    }
}