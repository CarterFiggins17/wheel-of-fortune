import javafx.animation.RotateTransition;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;


public class TheWheel {
    private int numOfArcs = 8;
    private Arc[] arrayArc = new Arc[numOfArcs];
    private Text[] arrayPoints = new Text[numOfArcs];
    private Circle wheel;
    private Polygon triangle;
    private Pane wheelPane = new Pane();
    private double middleX = 325;
    private double middleY = 325;
    private double radius = 195.0;
    private double holdR = 0;
    private double rotate = (360/numOfArcs);
    private RotateTransition[] arcSpin = new RotateTransition[numOfArcs];
    private RotateTransition[] pointSpin = new RotateTransition[numOfArcs];
    private double[] xPoints = new double[numOfArcs];
    private double[] yPoints = new double[numOfArcs];
    private int[] intPoints = new int[numOfArcs];
    private RotateTransition rt;
    private int cycle;
    private int spinNum;
    private int total;
    private int totalPoints;

    private Pane paneGame;

    public TheWheel(Pane pane){
        paneGame = pane;

        // makes pointer
        triangle = new Polygon( 315.0, 120.0, 335.0, 120.0, 325.0, 140.0);
//        triangle = new Polygon(middleX + 20 + radius, middleY - 20, middleX + radius, middleY, middleX + 20 + radius, middleY + 20);
        triangle.setFill(Color.BLUE);


        // makes all the arcs
        makeArcs();
        printArcs();
    }

    // makes all the arcs in wheel
    public void makeArcs() {

        for(int i = 0; i < numOfArcs; i++){
            Arc arc = new Arc();
            arc.setType(ArcType.ROUND);
            arc.setCenterX(middleX);
            arc.setCenterY(middleY);
            arc.setRadiusX(radius - 3);
            arc.setRadiusY(radius - 3);
            String values = Integer.toString((i+1) * 100);
            Text points = new Text(values);
            points.setStroke(Color.BLACK);
            points.setFill(Color.WHITE);
            points.setFont(Font.font(40));
            intPoints[i] = (i+1) * 100;

            double xPoint = (arc.getCenterX() - 30) + (radius - 70) * Math.cos(Math.toRadians(holdR + 250));
            double yPoint = arc.getCenterY() + (radius - 70) * Math.sin(Math.toRadians(holdR + 250));

            xPoints[i] = xPoint;
            yPoints[i] = yPoint;

            points.setY(yPoint);
            points.setX(xPoint);

            arc.setFill(Color.color(Math.random(), Math.random(), Math.random()));

            arc.setStartAngle(0 + holdR);
            arc.setLength(rotate);
            holdR += rotate;


            arrayArc[i] = arc;
            arrayPoints[i] = points;
        }

        rotate = 0;
        holdR = 0;
    }

    // paints all arcs
    public void printArcs(){

        for(int i = 0; i < numOfArcs; i++){
            wheelPane.getChildren().add(arrayArc[i]);

        }
        for(int i = 0; i< numOfArcs; i++){
            wheelPane.getChildren().add(arrayPoints[i]);
        }
        paneGame.getChildren().add(triangle);
    }

    // gets angle spin
    public int getSpin(){
        spinNum = (int) (Math.random() * 360 + 1);
        return spinNum + 360;
    }

    // rotates the wheel
    public void spinTheWheel(){
        cycle = getSpin();
        wheelPane.setRotate(0);
        rt = new RotateTransition(Duration.millis(3000), wheelPane);
        rt.setCycleCount(0);
        rt.setAutoReverse(false);
        rt.setByAngle(-cycle);
        rt.play();
    }

    // gets the numbers off wheel
    public int getPoints(){
        total = total + cycle;
        int degree = cycle % 360;
        int points = degree / 45;
        points = points + 1;
        if(points == 8){
            points = 0;
        }

        return intPoints[points];
    }

    public Pane getWheelPane(){
        return wheelPane;
    }



}